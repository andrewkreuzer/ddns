import boto3, logging

HOSTED_ZONE_ID = "Z1048057WQI0XPZ5GT3N"


def handler(event=None, context=None) -> dict:
    route53 = boto3.client('route53')

    if not event:
        return {'status': "no event"}

    dns_changes = {
        'Changes': [{
            'Action': 'UPSERT',
            'ResourceRecordSet': {
                'Name': "home.andrewkreuzer.com",
                'Type': 'A',
                'ResourceRecords': [{ 'Value': event["ip"] }],
                'TTL': 300,
            }
        }]
    }

    response = route53.change_resource_record_sets(
        HostedZoneId=HOSTED_ZONE_ID,
        ChangeBatch=dns_changes
    )

    info = {'status':response['ChangeInfo']['Status']}
    logging.info(info)
    return info
