"""An AWS Python Pulumi program"""

import json

import pulumi
import pulumi_aws as aws

iam_role_for_lambda = aws.iam.Role("lambdaRoleforDDNS",
    name="lambdaRoleforDDNS",
    assume_role_policy="""{
        "Version": "2012-10-17",
        "Statement": [
            {
                "Action": "sts:AssumeRole",
                "Principal": {
                    "Service": "lambda.amazonaws.com"
                    },
                "Effect": "Allow",
                "Sid": ""
                }
            ]
        }
    """)

iam_policy_for_lambda = aws.iam.Policy("lambdaPolicyForDDNS",
    name="lambdaPolicyForDDNS",
    path="/service-role/",
    description="Policy to allow ddns lambda to update home.andrewkreuzer.com record",
    policy=json.dumps({
        "Version": "2012-10-17",
        "Statement": [{
            "Action": ["route53:ChangeResourceRecordSets"],
            "Effect": "Allow",
            "Resource": "arn:aws:route53:::hostedzone/Z1048057WQI0XPZ5GT3N",
        }],
    })
)

role_policy_attach = aws.iam.RolePolicyAttachment("route53Update",
    role=iam_role_for_lambda.name,
    policy_arn=iam_policy_for_lambda.arn)

lambda_basic_execution_attach = aws.iam.RolePolicyAttachment("basicExecution",
    role=iam_role_for_lambda.name,
    policy_arn="arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole")

test_lambda = aws.lambda_.Function("ddns",
    name="ddns",
    code=pulumi.FileArchive("../lambda_src"),
    role=iam_role_for_lambda.arn,
    handler="main.handler",
    runtime="python3.9",
)
