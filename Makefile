lambda:
	cd infra && pulumi up

/var/lib/ddns/venv/bin/activate:
	python3 -m venv /var/lib/ddns/venv; \
	/var/lib/ddns/venv/bin/python3 -m pip install -r server_src/requirements.txt; \

/usr/local/bin/ddns:
	install -g root -o root server_src/main.py /usr/local/bin/ddns

/etc/systemd/system/ddns.service:
	install -g root -o root systemd/ddns.timer systemd/ddns.service /etc/systemd/system/; \
	systemctl enable ddns.service ddns.timer; \
	systemctl start ddns.timer

install: /usr/local/bin/ddns /var/lib/ddns/venv/bin/activate /etc/systemd/system/ddns.service

uninstall:
	systemctl disable ddns.service ddns.timer
	rm /etc/systemd/system/ddns.service /etc/systemd/system/ddns.timer
	rm /usr/local/bin/ddns
	rm -rdf /var/lib/ddns

.PHONY: lambda server_build
