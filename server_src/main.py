#!/usr/bin/python3

from botocore.config import Config
import boto3, json, logging

import requests

config = Config(
    region_name = 'us-east-2',
)

lambda_client = boto3.client('lambda', config=config)

r = requests.get('https://ifconfig.me')
ip = r.text
event = {"ip": ip}

response = lambda_client.invoke(
  FunctionName='ddns',
  Payload=json.dumps(event),
)

logging.info(f"New ip: {ip} function response: {response['Payload']}")
